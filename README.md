# MI-MVI
## Předzpracování dat
K předzpracování dat je potřeba několik souboru o velikosti několik desítek gigabytů. Tyto soubory jsou dostupné pouze ze serverů firmy Showmax. Lze demonstrovat na cvičení.

Popis předzpracovaných dat se nachází v souboru **data_describe.md**

## Učení CNN
Stačí spustit jupyter notebook CNN.ipynb, kterému stačí pouze dataset final.csv umístěný ve stejném adresáři jako je samotný notebook.