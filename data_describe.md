# Popis předzpracovaných dat

Celkem z předzpracování vzniklo 89 attributů.

Attribut | Popis
--- | ---
inorganic_flg | příznak zda se uživatel zaregistroval v rámci kampaně
win{0-27} | sedm časových oken reprezentující Po-Ne rozdělených ještě po 6ti hodinách, každé okno osahuje počet sekund koukání na videa
buffer_events{0-6} | sedm oken reprezentující Po-Ne, každé okno obsahuje počet buffering eventů za daný den
buffer_ratio{0-6} | opět sedm oken reprezentující Po-Ne, každé okno nese informaci kolik v průměru uživatel za daný den strávil bufferování videa
movies_ratio | kolik procent uživatel tráví času sledováním filmů z veškerých aktivit
tvshows_ratio | kolik procent uživatel tráví času sledováním seruálů/tv show z veškerých aktivit
online_ratio | procentuálně kolik uživatel shlédl videí online
offline_ratio | procentuálně kolik uživatel shlédl videí offline
search_ratio | poměr počtu prokliků na úspěšně vyhledaný výraz ku počtu hledání
browse_ratio | poměr počtu prokliků na konkrétní film/tv-show navigaci ku počtu eventů v navigaci
asset_download_count | počet stáhnutého contentu
watchlist_updated | počet úprav ve svém watchlistu resp. counter add/dell událostí
error_dialog | počet událostí, při které došlo k chybě
emails_delivered | počet odeslaných emailů uživateli
emails_opened | počet emailů, které si uživatel otevřel
emails_opened_vs_delivered_ratio | poměr otevřených emailů ku odeslaným emailům
uninstalled | příznak zda si uživatel odinstaloval aplikaci ze svého zařízení
genres | počet shlednutí u jednotlivých žánrů *(Action, Adventure, Animation, Comedy, CostumeDrama, Crime, CurrentAffairs, Dance, Documentary, Drama, Entertainment, Family, Fantasy, History, Horror, Lifestyle, Music, Mystery, Rom-com, Romance, Sci-Fi, Science, Sport, Stand-UpComedy, Thriller, War, Western, Wildlife)*
country_code_PL | příznak zda je uživatel z Polska
country_code_ZA | příznak zda je uživatel z Jihoafrické Republiky
payment_method_Trial_Paypal | příznak zda uživatel platil přes PayPal
payment_method_Trial_Cerdit_Card | přázak zda uživatel platil kreditní kartou



